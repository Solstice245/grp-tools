# grp-tools - Blender addon for StarCraft graphics output

**Blender** addon to facilitate automation of creating graphics in the **`.grp`** format, which is used in the game **StarCraft**.

Created by *John Wharton*

## Dependencies

* PMS (Python Modding Suite) (https://gitlab.com/Veeq7/PyMS-Veeq/-/tree/extended)
* Pillow (Python Imaging Library) (must be installed in Blender's own Python interpreter)

## Installation

1. Download the most recent version. **(https://gitlab.com/Solstice245/grp-tools/tree/master)**
2. Install the addon using one of these methods:
* __Option A__ (automatic):
	* From the Blender menu, navigate to `Edit -> Preferences -> Add-ons`.
	* Click on thje `Install` button and selected the *zipped* folder containing the addon.
* __Option B__ (manual):
	* Extract the script's folder from the zipped folder.
	* Place the newly made folder into the following location (Adjust for the installed version of Blender):
		* Windows:\
			`%APPDATA%\Blender Foundation\Blender\4.0\scripts\addons`
		* Linux:\
			`~/.config/blender/4.0/scripts/addons`
		* *(If the addon does not appear as a choice in Blender's preferences, click the `Refresh` button or restart the application.)*
3. Activate the addon in Blender's preferences by ticking the checkbox for the `GRP Tools` entry in the Add-ons tab.
4. Set the Python Modding Suite and StarCraft MPQ paths in the addon preferences to your PMS folder and desired output directory respectively.

## Usage

This addon adds the following panel to the ***Output*** tab of the properties editor:

* __GRP Tools__
	* *Operator*: `Output GRPs`
		* Executes the GRP output function of the addon. For each item in the GRP Config collection (described below) with the output checkbox ticked, a GRP file will be output to the GRP Config item path, relative to the MPQ path. (Set in addon preferences.)
		* *Flags*: `Preview Output`, `Preview Only`
			* If *Preview Output* is enabled, the script will try to instruct your system to open each spritesheet as they are completed in your default image viewer.
			* If *Preview Output* and *Preview Only* are enabled, the script will not write GRPs to file.
	* *List*: `GRP Config`
		* Each item represents a GRP output path. Has a checkbox for each item to toggle whether the given item will be output.
		* *Collection*: Will render only the contents of the selected collection. A collection must be selected.
		* *Catch Shadow*: If a mesh object is selected, the render output will be only the captured shadow of selected Collection. This object should NOT be a member of that collection.
		* *Color Palette*: Determines which color palette should be used when quantizing the output images for the selected item. The list of color palettes is populated from `.act` files found in the Python Modding Suite's `Palettes` sub-directory.
		* *Frame Range*: Determines which render frames will be included in the selected item's output.
		* *Resolution*: When specified as a non-zero value, the renderer's resolution settings will be overriden while outputting for the selected item.
		* *Render Angles*: Determines how many angles around any given animation frame should be rendered for the selected item. Can be animated in order to provide a variable number of directions for a frame.
		* *Static Angle*: The angle at which the model will be rendered when the *Static* render angle option is active.
		* *Preset Transformation*: Contains a list of options for modifications to be made to the scene or render output.
		* *Overlay Data*:
			* *Get LOB Data*: Opens a file selection window. Selecting a LOB file will import the data, which will be used to make an accurate Zerg Birth animation.
			* *List*: Overlay Output
			  * Each item represents an offset overlay file output path, which will be output when rendering this GRP. Has a checkbox for each item to toggle whether the given item will be output.
				* *List*: Objects
					* Each item can be assigned an object, whose position will be used to calculate an offset point in the offset overlay file. Has a checkbox for each item to toggle whether the given item will be calculated.
