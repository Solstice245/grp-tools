import bpy
import bpy_extras
from bpy.app.handlers import persistent
from PIL import Image as PIL_Image
import traceback
import math
import struct
import os
import glob
import subprocess
import time
import tempfile
import numpy as np

bl_info = {
	'name': 'GRP Tools',
	'description': 'Output StarCraft GRP files',
	'author': 'John Wharton',
	'version': (2, 0, 1),
	'blender': (4, 0, 0),
	'location': 'Output > GRP Tools',
	'warning': 'Requires StarCraft Python Modding Suite and Pillow',
	'doc_url': 'https://gitlab.com/Solstice245/grp-tools',
	'tracker_url': 'https://gitlab.com/Solstice245/grp-tools/-/issues',
	'category': 'Scene',
}


class PREFERENCES_PT_grpt(bpy.types.AddonPreferences):
	bl_idname = __package__

	def draw(self, context):
		scene = context.scene
		layout = self.layout
		layout.prop(scene, 'grpt_path_pms', text='Python Modding Suite Path')
		layout.prop(scene, 'grpt_path_mpq', text='Starcraft MPQ Path')

DESC_LOL_PATH = '''Filepath of the output overlay file (relative to MPQ archive directory)'''
DESC_LOL_OUTBOOL = '''
	Determines whether this overlay file will be rendered and output when the "Output GRPs" operation is executed\
'''

DESC_LOL_OB_OUTBOOL = '''
	Determines whether this object contributes to the overlay file that is output during the "Output GRPs" operation is executed\
'''

DESC_BMP_RESIZE = '''
	If set to a non-zero value, this value will be used to resize the output BMP. This does not affect render resolution\
'''

DESC_GRP_PATH = '''Filepath of the output GRP file (relative to MPQ archive directory)'''
DESC_GRP_PAL = '''Specifies which palette to quantize the render output with'''
DESC_GRP_COMPNODE = '''Specifies which composite node will be used for render output'''
DESC_GRP_OUTBOOL = '''
	Determines whether this GRP will be rendered and output when the "Output GRPs" operation is executed\
'''
DESC_GRP_COLLECTION = '''Specifies the collection which will be rendered out into the GRPs spritesheet'''
DESC_GRP_SIZE = '''
	If set to a non-zero value, this value will be used to determine the resolution of the GRP instead of the scene values\
'''
DESC_GRP_STATICANGLE = '''The angle at which the sprite will be rendered if it's a static facing frame'''
DESC_GRP_FRAMESTART = '''The frame which the renderer will start at for this GRP'''
DESC_GRP_FRAMENED = '''The frame which the renderer will end at for this GRP'''
DESC_GRP_SHADOWCATCHER = '''When set, the scene is temporarily modified to render only the shadows cast upon the specified object'''

ENUM_CAM_ANGLES = (
	('STATIC', 'Static', 'Renders 1 angle per frame (generally only used by structures)'),
	('RIGHT', 'Right', 'Renders 17 angles per frame, which the engine mirrors to create 32 directional movement'),
	('OMNI', 'Omni', 'Renders 32 angles per frame'),
)

GRP_preset_transformation = (
	('NONE', 'None', 'No modifications are made to the render output'),
	('ZBIRTHG', 'Zerg Birth (Ground)', 'Layers generic Zerg egg graphics which are for hatching ground units into the render output'),
	('ZBIRTHA', 'Zerg Birth (Air)', 'Layers generic Zerg egg graphics which are for hatching air units into the render output'),
	('ZMUTATEG', 'Zerg Mutate (Ground)', 'Layers generic Zerg egg graphics which are for mutating ground units into the render output'),
	('ZMUTATEA', 'Zerg Mutate (Air)', 'Layers generic Zerg egg graphics which are for mutating air units into the render output'),
	('ZBIRTHF', 'Zerg Birth (Flying)', 'Layers generic Zerg flyer egg graphics which are for hatching air units into the render output'),
)


def grp_frame_start_get(self):
	return self.get('frame_start', 0)


def grp_frame_start_set(self, value):
	self['frame_start'] = value
	self['frame_end'] = max(self.frame_end, value)


def grp_frame_end_get(self):
	return self.get('frame_end', 0)


def grp_frame_end_set(self, value):
	self['frame_end'] = max(self.frame_start, value)


class StringGroup(bpy.types.PropertyGroup):
	name: bpy.props.StringProperty(options=set())


class ObjectGroup(bpy.types.PropertyGroup):
	ob: bpy.props.PointerProperty(type=bpy.types.Object)
	output_bool: bpy.props.BoolProperty(options=set(), default=True, description=DESC_LOL_OB_OUTBOOL)


class GRP_LO_COORD(bpy.types.PropertyGroup):
	x: bpy.props.IntProperty(name='GRP Birth Offset X')
	y: bpy.props.IntProperty(name='GRP Birth Offset Y')


class GRP_LO_FIXED(bpy.types.PropertyGroup):
	coords: bpy.props.CollectionProperty(type=GRP_LO_COORD)


class GRP_LO_OB(bpy.types.PropertyGroup):
	output_path: bpy.props.StringProperty(options=set(), default='unit\\spritename.lol', description=DESC_LOL_PATH)
	output_bool: bpy.props.BoolProperty(options=set(), default=True, description=DESC_LOL_OUTBOOL)
	objects: bpy.props.CollectionProperty(type=ObjectGroup)
	objects_index: bpy.props.IntProperty(options=set(), default=-1)


class GRP_CONFIG(bpy.types.PropertyGroup):
	output_path: bpy.props.StringProperty(options=set(), default='unit\\spritename', description=DESC_GRP_PATH)
	output_bool: bpy.props.BoolProperty(options=set(), default=True, description=DESC_GRP_OUTBOOL)
	color_palette: bpy.props.StringProperty(options=set(), default='!gamebasic', description=DESC_GRP_PAL)
	collection: bpy.props.PointerProperty(type=bpy.types.Collection, description=DESC_GRP_COLLECTION)
	# composite_node: bpy.props.StringProperty(options=set(), description=DESC_GRP_COMPNODE)
	render_size_x: bpy.props.IntProperty(options=set(), min=0, max=256, default=0, description=DESC_GRP_SIZE)
	render_size_y: bpy.props.IntProperty(options=set(), min=0, max=256, default=0, description=DESC_GRP_SIZE)
	camera_angles: bpy.props.EnumProperty(options={'ANIMATABLE'}, name='GRP Render Angles', items=ENUM_CAM_ANGLES, default='OMNI')
	static_angle: bpy.props.FloatProperty(options={'ANIMATABLE'}, name='GRP Static Render Angle', subtype='ANGLE')
	frame_start: bpy.props.IntProperty(options=set(), description=DESC_GRP_FRAMESTART, get=grp_frame_start_get, set=grp_frame_start_set)
	frame_end: bpy.props.IntProperty(options=set(), description=DESC_GRP_FRAMENED, get=grp_frame_end_get, set=grp_frame_end_set)
	preset_transformation: bpy.props.EnumProperty(options=set(), items=GRP_preset_transformation, default='NONE')
	ob_shadow_catcher: bpy.props.PointerProperty(type=bpy.types.Object, description=DESC_GRP_SHADOWCATCHER)
	overlays_birth: bpy.props.CollectionProperty(type=GRP_LO_FIXED)
	overlay_output: bpy.props.CollectionProperty(type=GRP_LO_OB)
	overlay_output_index: bpy.props.IntProperty(options=set(), default=-1)


class UI_UL_grp_config(bpy.types.UIList):
	bl_idname = 'UI_UL_grpt_grp_config'

	def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index, flt_flag):
		if self.layout_type in {'DEFAULT', 'COMPACT'}:
			row = layout.row()
			row.prop(item, 'output_path', text='', emboss=False, icon_value=icon)
			row.prop(item, 'output_bool', text='', emboss=False, icon='CHECKBOX_HLT' if item.output_bool else 'CHECKBOX_DEHLT')


class UI_UL_lol_object(bpy.types.UIList):
	bl_idname = 'UI_UL_grpt_lol_object'

	def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index, flt_flag):
		if self.layout_type in {'DEFAULT', 'COMPACT'}:
			row = layout.row()
			row.prop_search(item, 'ob', context.scene, 'objects', text='', icon='OBJECT_DATA')
			row.separator()
			row.prop(item, 'output_bool', text='', emboss=False, icon='CHECKBOX_HLT' if item.output_bool else 'CHECKBOX_DEHLT')


class SCENE_PT_grpt(bpy.types.Panel):
	bl_idname = 'SCENE_PT_grpt'
	bl_label = 'GRP Tools'
	bl_space_type = 'PROPERTIES'
	bl_region_type = 'WINDOW'
	bl_context = 'output'
	bl_options = set()

	@classmethod
	def poll(cls, context):
		return context.scene

	def draw(self, context):
		layout = self.layout
		layout.use_property_split = True
		scene = context.scene

		layout.operator('grpt.bmp_output', icon='OUTPUT')
		layout.prop_search(
			scene, 'grpt_bmp_color_palette', context.scene, 'grpt_palettes',
			text='Color Palette', results_are_suggestions=True
		)
		row = layout.row(align=True)
		row.prop(scene, 'grpt_bmp_resize_x', text='Resize')
		row.prop(scene, 'grpt_bmp_resize_y', text='')
		layout.separator()

		layout.operator('grpt.grp_output', icon='OUTPUT')
		row = layout.row()
		row.use_property_split = False
		row.prop(scene, 'grpt_output_preview', text='Preview Output')
		sub = row.column()
		sub.active = scene.grpt_output_preview
		sub.prop(scene, 'grpt_preview_only', text='Preview Only')
		layout.separator()

		uilist_rows = 5 if len(scene.grpt_grp_config) else 3
		row = layout.row()
		row.template_list(
			'UI_UL_grpt_grp_config', 'grpt_grp_config', scene,
			'grpt_grp_config', scene, 'grpt_grp_output_index',
			rows=uilist_rows,
		)

		col = row.column(align=True)
		col.operator('grpt.grp_add', text='', icon='ADD')
		col.operator('grpt.grp_del', text='', icon='REMOVE')
		col.separator()
		col.operator('grpt.grp_mov', text='', icon='TRIA_UP').shift = -1
		col.operator('grpt.grp_mov', text='', icon='TRIA_DOWN').shift = 1

		if not len(scene.grpt_grp_config) or scene.grpt_grp_output_index < 0:
			return

		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]
		layout.prop(grp_config, 'collection', text='Collection')
		layout.prop(grp_config, 'ob_shadow_catcher', text='Catch Shadow')
		layout.prop_search(
			grp_config, 'color_palette', context.scene, 'grpt_palettes',
			text='Color Palette', results_are_suggestions=True
		)
		layout.separator()
		row = layout.row(align=True)
		row.prop(grp_config, 'frame_start', text='Frame Range')
		row.prop(grp_config, 'frame_end', text='')
		row = layout.row(align=True)
		row.prop(grp_config, 'render_size_x', text='Resolution')
		row.prop(grp_config, 'render_size_y', text='')
		layout.prop(grp_config, 'camera_angles', text='Render Angles')
		layout.prop(grp_config, 'static_angle', text='Static Angle')
		layout.separator()
		# layout.prop(grp_config, 'composite_node', text='Composite Node', icon='NODE')
		layout.prop(grp_config, 'preset_transformation', text='Preset Transformation')
		layout.separator()


class SCENE_PT_grpt_lol(bpy.types.Panel):
	bl_idname = 'SCENE_PT_grpt_lol'
	bl_parent_id = 'SCENE_PT_grpt'
	bl_label = 'Overlay Data'
	bl_space_type = 'PROPERTIES'
	bl_region_type = 'WINDOW'
	bl_context = 'output'
	bl_options = set()

	@classmethod
	def poll(cls, context):
		return len(context.scene.grpt_grp_config) and context.scene.grpt_grp_output_index >= 0

	def draw(self, context):
		layout = self.layout
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]

		layout.operator('grpt.grp_getlob')
		layout.separator()

		uilist_rows = 5 if len(grp_config.overlay_output) else 3
		row = layout.row()
		row.template_list(
			'UI_UL_grpt_grp_config', 'overlay_output', grp_config,
			'overlay_output', grp_config, 'overlay_output_index',
			rows=uilist_rows,
		)

		col = row.column(align=True)
		col.operator('grpt.grp_overlay_add', text='', icon='ADD')
		col.operator('grpt.grp_overlay_del', text='', icon='REMOVE')
		col.separator()
		col.operator('grpt.grp_overlay_mov', text='', icon='TRIA_UP').shift = -1
		col.operator('grpt.grp_overlay_mov', text='', icon='TRIA_DOWN').shift = 1

		if not len(grp_config.overlay_output) or grp_config.overlay_output_index < 0:
			return

		overlay = grp_config.overlay_output[grp_config.overlay_output_index]

		uilist_rows = 5 if len(overlay.objects) else 3
		row = layout.row()
		row.template_list(
			'UI_UL_grpt_lol_object', 'objects', overlay,
			'objects', overlay, 'objects_index',
			rows=uilist_rows,
		)

		col = row.column(align=True)
		col.operator('grpt.grp_overlay_object_add', text='', icon='ADD')
		col.operator('grpt.grp_overlay_object_del', text='', icon='REMOVE')
		col.separator()
		col.operator('grpt.grp_overlay_object_mov', text='', icon='TRIA_UP').shift = -1
		col.operator('grpt.grp_overlay_object_mov', text='', icon='TRIA_DOWN').shift = 1


def remove_collection_keyframes(prefix, index):
	for action in bpy.data.actions:
		path = f'{prefix}[{index}]'
		for fcurve in [fcurve for fcurve in action.fcurves if prefix in fcurve.data_path and path in fcurve.data_path]:
			action.fcurves.remove(fcurve)


def shift_collection_keyframes(prefix, index, offset=-1):
	for action in bpy.data.actions:
		path = f'{prefix}[{index}]'
		for fcurve in [fcurve for fcurve in action.fcurves if prefix in fcurve.data_path and path in fcurve.data_path]:
			fcurve.data_path = fcurve.data_path.replace(path, f'{prefix}[{index + offset}]')


def swap_collection_keyframes(prefix, old, new):
	for action in bpy.data.actions:
		path = f'{prefix}[{old}]'
		path_new = f'{prefix}[{new}]'

		fcurves = [fcurve for fcurve in action.fcurves if path in fcurve.data_path]
		fcurves_new = [fcurve for fcurve in action.fcurves if path_new in fcurve.data_path]

		for fcurve in fcurves:
			fcurve.data_path = fcurve.data_path.replace(path, path_new)

		for fcurve in fcurves_new:
			fcurve.data_path = fcurve.data_path.replace(path_new, path)


class SCENE_OT_grp_add(bpy.types.Operator):
	bl_idname = 'grpt.grp_add'
	bl_label = 'Add GRP Item'
	bl_description = 'Adds a new item to the collection'

	def invoke(self, context, event):
		scene = context.scene
		scene.grpt_grp_config.add()
		scene.grpt_grp_output_index = len(scene.grpt_grp_config) - 1
		return {'FINISHED'}


class SCENE_OT_grp_del(bpy.types.Operator):
	bl_idname = 'grpt.grp_del'
	bl_label = 'Remove GRP Item'
	bl_description = 'Removes the active item from the collection'

	def invoke(self, context, event):
		scene = context.scene

		if scene.grpt_grp_output_index not in range(len(scene.grpt_grp_config)):
			return {'FINISHED'}

		scene.grpt_grp_config.remove(scene.grpt_grp_output_index)

		remove_collection_keyframes('grpt_grp_config', scene.grpt_grp_output_index)
		for ii in range(scene.grpt_grp_output_index, len(scene.grpt_grp_config)):
			shift_collection_keyframes('grpt_grp_config', ii + 1)

		shift = 1 if scene.grpt_grp_output_index == len(scene.grpt_grp_config) else 0
		scene.grpt_grp_output_index = scene.grpt_grp_output_index - shift

		return {'FINISHED'}


class SCENE_OT_grp_mov(bpy.types.Operator):
	bl_idname = 'grpt.grp_mov'
	bl_label = 'Move GRP Item'
	bl_description = 'Moves the active item up/down in the collection'

	shift: bpy.props.IntProperty()

	def invoke(self, context, event):
		scene = context.scene

		if scene.grpt_grp_output_index not in range(len(scene.grpt_grp_config)):
			return {'FINISHED'}

		scene.grpt_grp_config.move(scene.grpt_grp_output_index, scene.grpt_grp_output_index + self.shift)
		swap_collection_keyframes('grpt_grp_config', scene.grpt_grp_output_index, scene.grpt_grp_output_index + self.shift)
		scene.grpt_grp_output_index += self.shift

		return {'FINISHED'}


class SCENE_OT_grp_get_lob(bpy.types.Operator):
	bl_idname = 'grpt.grp_getlob'
	bl_label = 'Get LOB Data'
	bl_description = 'Retrieves frame offset data from the selected file'

	filename_ext = '.lo*'
	filter_glob: bpy.props.StringProperty(options={'HIDDEN'}, default='*.lo*;')
	filepath: bpy.props.StringProperty(name='File Path', description='File path for import operation', maxlen=1023, default='')

	def invoke(self, context, event):
		context.window_manager.fileselect_add(self)
		return {'RUNNING_MODAL'}

	def execute(self, context):
		frame_data = []
		with open(self.filepath, 'rb') as lo:
			try:
				frame_count, overlay_count = struct.unpack('<II', lo.read(8))
				overlay_data_offsets = struct.unpack(f'<{frame_count}I', lo.read(4*frame_count))
				frame_data = []
				for offset in overlay_data_offsets:
					lo.seek(offset)
					frame_data.append(tuple(struct.iter_unpack('bb', lo.read(2*overlay_count))))
			except struct.error:
				error_msg = f'LO file {os.path.basename(self.filepath)} is not formatted correctly and was unable to be read'
				self.report({'ERROR'}, error_msg)
				return {'CANCELLED'}

		grp_config = context.scene.grpt_grp_config[context.scene.grpt_grp_output_index]

		# clear existing data
		for ii in range(len(grp_config.overlays_birth)):
			grp_config.overlays_birth.remove(0)

		# insert new data
		for ii in range(overlay_count):
			overlay = grp_config.overlays_birth.add()
			# we're only interested in the last one
			coord = overlay.coords.add()
			coord.x, coord.y = frame_data[-1][ii]

		return {'FINISHED'}


class SCENE_OT_grp_overlay_add(bpy.types.Operator):
	bl_idname = 'grpt.grp_overlay_add'
	bl_label = 'Add GRP Overlay Item'
	bl_description = 'Adds a new item to the collection'

	def invoke(self, context, event):
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]

		grp_config.overlay_output.add()
		grp_config.overlay_output_index = len(grp_config.overlay_output) - 1
		return {'FINISHED'}


class SCENE_OT_grp_overlay_del(bpy.types.Operator):
	bl_idname = 'grpt.grp_overlay_del'
	bl_label = 'Remove GRP Overlay Item'
	bl_description = 'Removes the active item from the collection'

	def invoke(self, context, event):
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]

		if grp_config.overlay_output_index not in range(len(grp_config.overlay_output)):
			return {'FINISHED'}

		grp_config.overlay_output.remove(grp_config.overlay_output_index)
		shift = 1 if grp_config.overlay_output_index == len(grp_config.overlay_output) else 0
		grp_config.overlay_output_index = grp_config.overlay_output_index - shift

		return {'FINISHED'}


class SCENE_OT_grp_overlay_mov(bpy.types.Operator):
	bl_idname = 'grpt.grp_overlay_mov'
	bl_label = 'Move GRP Overlay Item'
	bl_description = 'Moves the active item up/down in the collection'

	shift: bpy.props.IntProperty()

	def invoke(self, context, event):
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]

		if grp_config.overlay_output_index not in range(len(grp_config.overlay_output)):
			return {'FINISHED'}

		grp_config.overlay_output.move(grp_config.overlay_output_index, grp_config.overlay_output_index + self.shift)
		grp_config.overlay_output_index += self.shift

		return {'FINISHED'}


class SCENE_OT_grp_overlay_object_add(bpy.types.Operator):
	bl_idname = 'grpt.grp_overlay_object_add'
	bl_label = 'Add GRP Overlay Object Item'
	bl_description = 'Adds a new item to the collection'

	def invoke(self, context, event):
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]
		overlay = grp_config.overlay_output[grp_config.overlay_output_index]

		overlay.objects.add()
		overlay.objects_index = len(scene.grpt_grp_config) - 1
		return {'FINISHED'}


class SCENE_OT_grp_overlay_object_del(bpy.types.Operator):
	bl_idname = 'grpt.grp_overlay_object_del'
	bl_label = 'Remove GRP Overlay Object Item'
	bl_description = 'Removes the active item from the collection'

	def invoke(self, context, event):
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]
		overlay = grp_config.overlay_output[grp_config.overlay_output_index]

		if overlay.objects_index not in range(len(overlay.objects)):
			return {'FINISHED'}

		overlay.objects.remove(overlay.objects_index)
		shift = 1 if overlay.objects_index == len(overlay.objects) else 0
		overlay.objects_index = overlay.objects_index - shift

		return {'FINISHED'}


class SCENE_OT_grp_overlay_object_mov(bpy.types.Operator):
	bl_idname = 'grpt.grp_overlay_object_mov'
	bl_label = 'Move GRP Overlay Object Item'
	bl_description = 'Moves the active item up/down in the collection'

	shift: bpy.props.IntProperty()

	def invoke(self, context, event):
		scene = context.scene
		grp_config = scene.grpt_grp_config[scene.grpt_grp_output_index]
		overlay = grp_config.overlay_output[grp_config.overlay_output_index]

		if overlay.objects_index not in range(len(overlay.objects)):
			return {'FINISHED'}

		overlay.objects.move(overlay.objects_index, overlay.objects_index + self.shift)
		overlay.objects_index += self.shift

		return {'FINISHED'}


class SCENE_OT_grp_bmp_output(bpy.types.Operator):
	bl_idname = 'grpt.bmp_output'
	bl_label = 'Output BMP'
	bl_description = 'Renders the scene using the currently selected frame, and outputs a BMP file using the default render output path, and the given color palette'
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.scene

	def op_init(self):
		self.user_file_format = self.scene.render.image_settings.file_format
		self.user_color_depth = self.scene.render.image_settings.color_depth
		self.user_compression = self.scene.render.image_settings.compression
		self.user_color_mode = self.scene.render.image_settings.color_mode

		# overwrite user defined render settings
		self.scene.render.image_settings.file_format = 'PNG'
		self.scene.render.image_settings.color_mode = 'RGB'
		self.scene.render.image_settings.color_depth = '16'  # higher depth needed to ensure proper quantization
		self.scene.render.image_settings.compression = 0  # maximize write speed of PNGs

	def op_finish(self):
		self.scene.render.image_settings.file_format = self.user_file_format
		self.scene.render.image_settings.color_depth = self.user_color_depth
		self.scene.render.image_settings.compression = self.user_compression
		self.scene.render.image_settings.color_mode = self.user_color_mode

	def execute(self, context):
		scene = context.scene
		self.scene = context.scene

		self.op_init()

		pal_ext = os.extsep + 'act'
		if scene.grpt_bmp_color_palette and not scene.grpt_bmp_color_palette.endswith(pal_ext):
			pal_filename = scene.grpt_bmp_color_palette + pal_ext
		else:
			pal_filename = scene.grpt_bmp_color_palette

		err_strings = []

		try:
			with open(os.path.join(scene.grpt_path_pms, 'Palettes', pal_filename), 'rb') as palette:
				try:
					palette_data = struct.unpack('<768B', palette.read(768))
				except struct.error:
					err_strings.append(f'Tried to reference a palette file "{pal_filename}" which is formatted incorrectly')
		except OSError:
			err_strings.append(
				f'Tried to reference a palette file "{pal_filename}" which does not exist in or is inacessible from the PMS path'
			)

		if err_strings:
			self.report({'ERROR'}, 'The following problems occurred during the BMP output operation:\n' + '\n'.join(self.err_strings))

			self.op_finish()
			return {'FINISHED'}

		hexcodes = []
		for ii in range(0, len(palette_data), 3):
			hexcodes.append(hex(palette_data[ii])[2:].zfill(2) + hex(palette_data[ii + 1])[2:].zfill(2) + hex(palette_data[ii + 2])[2:].zfill(2))

		bpy.ops.render.render(animation=False, use_viewport=False, write_still=True)
		output_im = PIL_Image.open(scene.render.filepath + (os.extsep + 'png' if not scene.render.filepath.endswith(os.extsep + 'png') else ''))
		output_im.load()

		# GRP is not RGB, so we quantize the sprite sheet using the color table
		palette_im = pil_get_palette_data(palette_data)
		output_new_im = output_im.im.convert('P', 1, palette_im.im)
		output_new_im = output_im._new(output_new_im)

		if scene.grpt_bmp_color_palette == '!unit_wirefram':
			width, height = output_im.size
			for x in range(width):
				for y in range(height):
					cpixel = output_im.getpixel((x, y))
					hexcode = hex(cpixel[0])[2:].zfill(2) + hex(cpixel[1])[2:].zfill(2) + hex(cpixel[2])[2:].zfill(2)
					ipixel = output_new_im.getpixel((x, y))
					if hexcode in ('fcfc39', 'fdfc39', '10fc19', 'f88d14', 'c81819') and ipixel in (208, 209, 210, 211):
						output_new_im.putpixel((x, y), output_new_im.getpixel((x, y)) + 8)

		if scene.grpt_bmp_resize_x and scene.grpt_bmp_resize_y:
			output_new_im = output_new_im.resize((scene.grpt_bmp_resize_x, scene.grpt_bmp_resize_y))

		output_new_im.save(os.path.splitext(scene.render.filepath)[0] + os.extsep + 'bmp')
		os.remove(scene.render.filepath + (os.extsep + 'png' if not scene.render.filepath.endswith(os.extsep + 'png') else ''))

		self.op_finish()
		return {'FINISHED'}


class SCENE_OT_grp_output(bpy.types.Operator):
	bl_idname = 'grpt.grp_output'
	bl_label = 'Output GRPs'
	bl_description = 'Renders the scene for all GRP entries marked for output within their respective frame ranges'
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		return context.scene

	def op_init(self):
		# saving settings which may be modified by the script so that they can be restored later
		self.user_frame_current = self.scene.frame_current
		self.user_camera = self.scene.camera
		self.user_file_path = self.scene.render.filepath
		self.user_file_format = self.scene.render.image_settings.file_format
		self.user_color_depth = self.scene.render.image_settings.color_depth
		self.user_compression = self.scene.render.image_settings.compression
		self.user_res_x = self.scene.render.resolution_x
		self.user_res_y = self.scene.render.resolution_y
		self.user_collection_exclude = {child: child.exclude for child in self.view_layer.layer_collection.children}

		self.err_strings = []  # list of strings for reporting warnings or problems

		# self.composite_nodes = []
		# if self.scene.use_nodes:
		# 	for node in self.scene.node_tree.nodes:
		# 		if node.bl_static_type == 'COMPOSITE':
		# 			self.composite_nodes.append(node)

		self.render_cam = self.user_camera.data.copy()
		self.render_cam_ob = self.user_camera.copy()
		self.render_cam_ob.data = self.render_cam
		self.scene.collection.objects.link(self.render_cam_ob)
		self.scene.camera = self.render_cam_ob
		self.scene.camera.rotation_euler.z = 0

		if self.scene.camera.parent:
			self.user_camera_parent_rot_z = self.scene.camera.parent.rotation_euler.z
			self.scene.camera.parent.rotation_euler.z = 0

		# overwrite user defined render settings
		self.scene.render.image_settings.file_format = 'PNG'
		self.scene.render.image_settings.color_depth = '8'  # higher depths not needed
		self.scene.render.image_settings.compression = 0  # maximize write speed of PNGs

		for child in self.view_layer.layer_collection.children:
			child.exclude = True

	def op_finish(self):
		# restoring user settings
		self.scene.frame_set(self.user_frame_current)
		self.scene.camera = self.user_camera
		self.scene.render.filepath = self.user_file_path
		self.scene.render.image_settings.file_format = self.user_file_format
		self.scene.render.image_settings.color_depth = self.user_color_depth
		self.scene.render.image_settings.compression = self.user_compression
		self.scene.render.resolution_x = self.user_res_x
		self.scene.render.resolution_y = self.user_res_y

		if self.scene.camera.parent:
			self.scene.camera.parent.rotation_euler.z = self.user_camera_parent_rot_z

		for child in self.user_collection_exclude:
			child.exclude = self.user_collection_exclude[child]

		if (self.err_strings):
			self.report({'ERROR'}, 'The following problems occurred during the GRP output operation:\n' + '\n'.join(self.err_strings))

		bpy.data.objects.remove(self.render_cam_ob)
		bpy.data.cameras.remove(self.render_cam)

	def execute(self, context):
		t_exec = time.time()

		self.scene = context.scene
		self.view_layer = context.view_layer

		if not self.scene.camera:
			self.report({'ERROR'}, 'Please select a camera for the scene before running this operation')
			return {'CANCELLED'}

		self.op_init()

		if not len(self.scene.grpt_grp_config):
			return {'CANCELLED'}

		try:
			for grp_index, grp_config in enumerate(self.scene.grpt_grp_config):
				self.grp_output(grp_index, grp_config)
		except Exception as e:
			print(traceback.format_exc())
		finally:
			self.op_finish()
			print('Total output time:', time.time() - t_exec)
			return {'FINISHED'}

	def grp_output_init(self):
		self.clones = []  # list of collections that are duplicated for parallel rendering
		self.obj_clone_dicts = {}  # maps clone objects back to the original object
		self.overlay_data = {}  # data for LO* data export
		self.palette_data = self.palette_data_get()

		# self.composite_node = None
		# if self.scene.use_nodes:
		# 	self.composite_node = self.scene.node_tree.nodes.get(self.grp.composite_node)
		# if self.composite_node:
		# 	for node in self.composite_nodes:
		# 		node.mute = False
		# 	for node in self.composite_nodes:
		# 		if node is not self.composite_node:
		# 			node.mute = True

		camera_angles_fcurve = None
		if self.scene.animation_data:
			if self.scene.animation_data.action:
				camera_angles_fcurve = self.scene.animation_data.action.fcurves.find(f'grpt_grp_config[{self.index}].camera_angles')
			elif self.scene.animation_data.drivers:
				camera_angles_fcurve = self.scene.animation_data.drivers.find(f'grpt_grp_config[{self.index}].camera_angles')

		self.columns = 1 + self.grp.frame_end - self.grp.frame_start

		self.column_to_rows = []
		if camera_angles_fcurve:
			for f in range(self.grp.frame_start, 1 + self.grp.frame_end):
				y = camera_angles_fcurve.evaluate(f)
				val = 1 if y == 0 else 17 if y == 1 else 32
				self.column_to_rows.append(val)
			self.rows = max(self.column_to_rows)
		else:
			y = self.grp.camera_angles
			self.rows = 1 if y == 'STATIC' else 17 if y == 'RIGHT' else 32
			self.column_to_rows = [self.rows] * self.columns

		# TODO GRPs can have bounds greater than 256 (but not individual frames)
		# TODO implement some way to specify frame offsets?
		self.sprite_size_x = min(self.grp.render_size_x or self.user_res_x, 256)
		self.sprite_size_y = min(self.grp.render_size_y or self.user_res_y, 256)
		self.render_filepaths = [os.path.join(tempfile.gettempdir(), 'grp_' + str(f).zfill(4) + os.extsep + 'png') for f in range(self.columns)]
		self.spritesheet_filepath = os.path.join(tempfile.gettempdir(), f'grpt_spritesheet{os.extsep}bmp')

		transform_dict = {
			'ZBIRTHG': (7, 'img\\zerg-birth-ground'), 'ZBIRTHA': (7, 'img\\zerg-birth-air'), 'ZBIRTHF': (7, 'img\\zerg-birth-flyer'),
			'ZMUTATEG': (8, 'img\\zerg-mutate-ground'), 'ZMUTATEA': (6, 'img\\zerg-mutate-air')
		}
		self.req_columns, self.transform_dir = transform_dict.get(self.grp.preset_transformation, (None, None))
		if self.transform_dir:
			self.transform_dir = os.path.join(os.path.dirname(__file__), self.transform_dir)

		if self.grp.ob_shadow_catcher:
			self.user_shadow_render = self.grp.ob_shadow_catcher.hide_render
			self.render_shadow_ob = self.grp.ob_shadow_catcher.copy()
			self.render_shadow_ob.hide_render = False
			self.render_shadow_ob.scale.x *= self.rows
			self.scene.collection.objects.link(self.render_shadow_ob)
			self.grp.ob_shadow_catcher.hide_render = True
		else:
			self.render_shadow_ob = None

	def grp_output_final(self):
		for clone in self.clones:
			for ob in reversed(clone.all_objects):
				bpy.data.objects.remove(ob)
			bpy.data.collections.remove(clone)

		if self.grp.ob_shadow_catcher:
			self.grp.ob_shadow_catcher.hide_render = self.user_shadow_render
			bpy.data.objects.remove(self.render_shadow_ob)

		# for node in self.composite_nodes:
		# 	node.mute = False

	def grp_output(self, grp_index, grp_config):
		t_grp = time.time()

		self.index = grp_index
		self.grp = grp_config

		if not self.grp.output_bool:
			return

		self.grp_output_init()

		if self.grp_abort():
			self.grp_output_final()
			print('Abort GRP:', self.grp.output_path)
			return

		try:
			print('Output GRP:', self.grp.output_path)

			clone_data = collection_clones_get(self.grp.collection, self.rows)

			self.clones = [clone_[0] for clone_ in clone_data]
			self.obj_clone_dicts = [clone_[1] for clone_ in clone_data]

			for ii, clone in enumerate(self.clones):
				clone.all_objects[0].location.x += ii * self.user_camera.data.ortho_scale
				clone.all_objects[0].rotation_euler.z = math.radians(-ii * 11.25 + 180)

			for child in self.view_layer.layer_collection.children:
				if child.name in [clone.name for clone in self.clones]:
					child.exclude = False
					if self.grp.ob_shadow_catcher:
						child.indirect_only = True

			self.scene.camera.data.ortho_scale = self.user_camera.data.ortho_scale * self.rows
			self.scene.render.resolution_x = self.sprite_size_x * self.rows
			self.scene.render.resolution_y = self.sprite_size_y

			print(f'GRP {self.grp.output_path} prep time:', time.time() - t_grp)

			t_render = time.time()
			self.render_frames()

			print(f'GRP {self.grp.output_path} render time:', time.time() - t_render)

			t_process = time.time()

			# for when you have graphics being layered below or on top of the render output
			self.pil_grp_transform_frames()

			palette_im = pil_get_palette_data(self.palette_data)

			# assembling render output into a single sprite sheet and then quantizing using the palette
			spritesheet_im = self.pil_grp_spritesheet_save(palette_im)

			# opens system image viewer
			if self.scene.grpt_output_preview:
				self.pil_grp_spritesheet_preview(palette_im)

			palette_im.close()

			print(f'GRP {self.grp.output_path} processing time:', time.time() - t_process)

			if not (self.scene.grpt_output_preview and self.scene.grpt_preview_only):
				t_convert = time.time()
				# making command line call to the PMS python script pyGRP
				# pyGRP handles the actual conversion from the render output to GRP
				py_grp = os.path.join(self.scene.grpt_path_pms, 'pyGRP.pyw')
				grp_path = os.path.join(self.scene.grpt_path_mpq, self.grp.output_path) + os.extsep + 'grp'

				command = [
					'py', '-2.7', f'{py_grp}',  # caller
					'-b', f'-r 1', f'-f {sum(self.column_to_rows)}',  # options
					f'"{self.spritesheet_filepath}"', f'"{grp_path}"',  # arguments
				]

				subprocess.call(command, shell=True)
				print(f'GRP {self.grp.output_path} conversion time:', time.time() - t_convert)

			self.lol_output()
			print(f'GRP {self.grp.output_path} total time:', time.time() - t_grp)

		except Exception as e:
			print(traceback.format_exc())

		finally:
			self.grp_output_final()

	def grp_abort(self):

		if not self.grp.collection:
			self.err_strings.append(f'GRP {self.grp.output_path} has no assigned collection')
			return True

		if not self.grp.color_palette:
			self.err_strings.append(f'GRP {self.grp.output_path} has no assigned color palette')
			return True

		if self.transform_dir:

			if self.rows != 1 or len(set(self.column_to_rows)) > 1:
				self.err_strings.append(f'GRP {self.grp.output_path} preset transformation is supported only for static facing GRPs')
				return True

			if not len(self.column_to_rows) == self.req_columns:
				self.err_strings.append(f'GRP {self.grp.output_path} sequence length ({len(self.column_to_rows)}) does not match the preset length of {self.req_columns}')
				return True

		if self.palette_data is None:
			return True

		return False

	def render_frames(self):
		# this line is needed to relibably compute world_to_camera_view accurately on the first iteration
		self.scene.camera.location.x = self.user_camera.location.x + self.user_camera.data.ortho_scale * (self.rows / 2 - 0.5)

		for overlay in self.grp.overlay_output:

			if not overlay.output_bool:
				continue

			if not len([oob for oob in overlay.objects if oob.output_bool]):
				continue

			self.overlay_data[overlay] = []

		img_counter = 0

		for f, rows in enumerate(self.column_to_rows):

			for clone in self.clones[rows:]:
				clone.hide_render = True

			for clone in self.clones[:rows]:
				clone.hide_render = False

			self.scene.frame_set(f + self.grp.frame_start)

			# setting this after each frame_set() so that it overrides the animated value for X only
			self.scene.camera.location.x = self.user_camera.location.x + self.user_camera.data.ortho_scale * (self.rows / 2 - 0.5)

			if self.render_shadow_ob:
				self.render_shadow_ob.location.x = self.grp.ob_shadow_catcher.location.x + self.user_camera.data.ortho_scale * (self.rows / 2 - 0.5)

			self.clones[0].all_objects[0].rotation_euler.z = self.grp.static_angle if rows == 1 else math.radians(180)

			for clone in self.clones:
				# seems unnecessary, but for some reason relative parenting of some objects breaks if this line is absent
				clone.all_objects[0].location.z = clone.all_objects[0].location.z
				clone.all_objects[0].rotation_euler.z = clone.all_objects[0].rotation_euler.z

			for col in range(self.rows):
				clone = self.clones[col]
				obj_clone_dict = self.obj_clone_dicts[col]

				for overlay in self.overlay_data:
					for oob in overlay.objects:
						co_2d = bpy_extras.object_utils.world_to_camera_view(self.scene, self.scene.camera, obj_clone_dict[oob.ob].matrix_world.translation)
						self.overlay_data[overlay].append(round(co_2d.x * self.scene.render.resolution_x - (col + 0.5) * self.sprite_size_x))
						self.overlay_data[overlay].append(-round(co_2d.y * self.scene.render.resolution_y - self.sprite_size_y * 0.5))

			self.scene.render.filepath = self.render_filepaths[f]
			bpy.ops.render.render(animation=False, use_viewport=False, write_still=True)

	def lol_output(self):

		if (self.scene.grpt_output_preview and self.scene.grpt_preview_only):
			return

		for overlay in self.overlay_data:
			overlay_data = self.overlay_data[overlay]
			frame_count = len(overlay_data) // 2 // len(overlay.objects)
			overlay_count = len(overlay.objects)

			header_end_offset = (8 + (4 * frame_count))
			frame_data_size = 2 * overlay_count
			overlay_data_offsets = [header_end_offset + frame_data_size * ii for ii in range(frame_count)]
			header_data = frame_count, overlay_count, *overlay_data_offsets

			buffer = struct.pack(f'<{len(header_data)}I{len(overlay_data)}b', *header_data, *overlay_data)

			with open(os.path.join(self.scene.grpt_path_mpq, overlay.output_path), 'w+b') as lo:
				lo.write(buffer)

			print('LO', os.path.join(self.scene.grpt_path_mpq, overlay.output_path))

	def palette_data_get(self):
		pal_ext = os.extsep + 'act'
		if self.grp.color_palette and not self.grp.color_palette.endswith(pal_ext):
			pal_filename = self.grp.color_palette + pal_ext
		else:
			pal_filename = self.grp.color_palette

		try:
			with open(os.path.join(self.scene.grpt_path_pms, 'Palettes', pal_filename), 'rb') as palette:
				try:
					return struct.unpack('<768B', palette.read(768))
				except struct.error:
					self.err_strings.append(
						f'GRP {self.grp.output_path} tried to reference a palette file "{pal_filename}" which is formatted incorrectly'
					)
		except OSError:
			self.err_strings.append(
				f'GRP {self.grp.output_path} tried to reference a palette file \
				"{pal_filename}" which does not exist in or is inacessible from the PMS path'
			)

	def pil_grp_transform_frames(self):
		if not self.transform_dir:
			return

		if self.grp.preset_transformation in ('ZBIRTHG', 'ZBIRTHA', 'ZBIRTHF'):
			x_vals = [0]
			y_vals = [0]
			for overlay in self.grp.overlays_birth:
				x_vals.extend([coord.x for coord in overlay.coords])
				y_vals.extend([coord.y for coord in overlay.coords])

			x_offset_min = min(x_vals)
			x_offset_max = max(x_vals)
			x_offset_size_mod = (abs(x_offset_min) + x_offset_max) * 2

			y_offset_min = min(y_vals)
			y_offset_max = max(y_vals)
			y_offset_size_mod = (abs(y_offset_min) + y_offset_max) * 2

			self.sprite_size_x += x_offset_size_mod
			self.sprite_size_y += y_offset_size_mod

		for ii, filepath in enumerate(self.render_filepaths):

			try:
				over_im = PIL_Image.open(os.path.join(self.transform_dir, f'over{str(ii)}{os.extsep}png'))
				over_im.load()
				self.sprite_size_x = max(over_im.size[0], self.sprite_size_x)
				self.sprite_size_y = max(over_im.size[1], self.sprite_size_y)
			except OSError:
				over_im = None  # probably doesn't exist

			try:
				under_im = PIL_Image.open(os.path.join(self.transform_dir, f'under{str(ii)}{os.extsep}png'))
				under_im.load()
				self.sprite_size_x = max(under_im.size[0], self.sprite_size_x)
				self.sprite_size_y = max(under_im.size[1], self.sprite_size_y)
			except OSError:
				under_im = None  # probably doesn't exist

			if under_im is None and over_im is None:
				continue

			frame_im = pil_frame_rgba(filepath)

			new_im = PIL_Image.new('RGBA', (self.sprite_size_x, self.sprite_size_y), (0, 0, 0, 0))

			if under_im:
				new_im.alpha_composite(under_im, ((self.sprite_size_x - under_im.size[0]) // 2, (self.sprite_size_y - under_im.size[1]) // 2))
				under_im.close()

			if self.grp.preset_transformation in ('ZBIRTHG', 'ZBIRTHA', 'ZBIRTHF') and len(self.grp.overlays_birth):
				mid_x = (self.sprite_size_x - frame_im.size[0]) // 2
				mid_y = (self.sprite_size_y - frame_im.size[1]) // 2
				interp_fac = (ii + 1) / self.req_columns
				for overlay in self.grp.overlays_birth:
					co = overlay.coords[-1]
					new_im.alpha_composite(frame_im, (round(mid_x + (co.x * interp_fac)), round(mid_y + (co.y * interp_fac))))
			else:
				new_im.alpha_composite(frame_im, ((self.sprite_size_x - frame_im.size[0]) // 2, (self.sprite_size_y - frame_im.size[1]) // 2))

			frame_im.close()

			if over_im:
				new_im.alpha_composite(over_im, ((self.sprite_size_x - over_im.size[0]) // 2, (self.sprite_size_y - over_im.size[1]) // 2))
				over_im.close()

			new_im.save(filepath)
			new_im.close()

	def pil_grp_spritesheet_save(self, palette_im):
		spritesheet_im = PIL_Image.new('RGB', (self.sprite_size_x, self.sprite_size_y * sum(self.column_to_rows)))

		img_counter = 0
		for filename, column_rows in zip(self.render_filepaths, self.column_to_rows):
			frame_im = PIL_Image.open(filename)
			for jj in range(column_rows):
				crop_im = frame_im.crop((jj * self.sprite_size_x, 0, (jj + 1) * self.sprite_size_x, self.sprite_size_y))
				spritesheet_im.paste(crop_im, (0, img_counter * self.sprite_size_y))
				img_counter += 1
			frame_im.close()

		spritesheet_im.load()
		# GRP is not RGB, so we quantize the sprite sheet using the color table
		spritesheet_new_im = spritesheet_im.im.convert('P', 0, palette_im.im)
		spritesheet_new_im = spritesheet_im._new(spritesheet_new_im)
		spritesheet_new_im.save(self.spritesheet_filepath)
		spritesheet_im.close()
		spritesheet_new_im.close()

	def pil_grp_spritesheet_preview(self, palette_im):
		preview_im = PIL_Image.new('RGB', (self.sprite_size_x * self.rows, self.sprite_size_y * self.columns))

		for ii, filename in enumerate(self.render_filepaths):
			frame_im = PIL_Image.open(filename)
			preview_im.paste(frame_im, (0, ii * self.sprite_size_y))
			frame_im.close()

		preview_name = os.path.join(tempfile.gettempdir(), f'grpt_preview{os.extsep}bmp')

		preview_im.load()
		preview_new_im = preview_im.im.convert('P', 0, palette_im.im)
		preview_new_im = preview_im._new(preview_new_im)
		preview_new_im.save(preview_name)

		preview_im = preview_new_im.im.convert('RGB')
		preview_im = preview_new_im._new(preview_im)
		preview_im.show()
		preview_im.close()


def pil_get_palette_data(palette_data):
	palette_im = PIL_Image.new('P', (16, 16))
	palette_im.putpalette(palette_data)
	palette_im.load()
	return palette_im


def pil_frame_rgba(filename):
	frame_im = PIL_Image.open(filename)
	frame_im.load()
	frame_ima = frame_im.im.convert('RGBA')
	frame_im = frame_im._new(frame_ima)
	r, g, b, a = np.rollaxis(np.asarray(frame_im), axis=-1)
	r_m = r > 1
	g_m = g > 1
	b_m = b > 1
	# combine the three masks using the binary "or" operation
	a = a * ((r_m == 1) | (g_m == 1) | (b_m == 1))
	frame_im.close()
	# stack the img back together
	return PIL_Image.fromarray(np.dstack([r, g, b, a]), 'RGBA')


def collection_clones_get(collection, count):
	clones = []
	for ii in range(count):
		clones.append(clone_collection(collection))
	return clones


def clone_collection(src):
	scene = bpy.context.scene
	clone_collec = bpy.data.collections.new('clone')
	scene.collection.children.link(clone_collec)

	obj_clone_dict = {}

	for obj in src.objects:
		if (obj.parent == None):
			clone_object_recursive(src, obj, clone_collec, obj_clone_dict)

	for obj in obj_clone_dict:
		clone = obj_clone_dict[obj]
		for modifier in clone.modifiers:
			for propname in dir(modifier):
				if '__' not in propname:
					value = getattr(modifier, propname)
					if type(value) == bpy.types.Object:
						setattr(modifier, propname, obj_clone_dict.get(value, value))

	for obj in obj_clone_dict:
		clone = obj_clone_dict[obj]
		for constraint in clone.constraints:
			for propname in dir(constraint):
				if '__' not in propname:
					value = getattr(constraint, propname)
					if type(value) == bpy.types.Object:
						setattr(constraint, propname, obj_clone_dict.get(value, value))
			if hasattr(constraint, 'targets'):
				for target in constraint.targets:
					target.target = obj_clone_dict.get(target.target, target.target)

	for obj in obj_clone_dict:
		if not obj.animation_data:
			continue
		clone = obj_clone_dict[obj]
		if len(obj.animation_data.drivers) and clone.animation_data == None:
			clone.animation_data_create()
		for fcurve in clone.animation_data.drivers:
			for var in fcurve.driver.variables:
				if hasattr(constraint, 'targets'):
					for target in var.targets:
						if type(target.id) == bpy.types.Object:
							target.id = obj_clone_dict.get(target.id, target.id)

	return clone_collec, obj_clone_dict


def clone_object_recursive(src, obj, clone_collec, obj_clone_dict):
	if obj.name not in src.all_objects:
		return

	clone = obj.copy()
	obj_clone_dict[obj] = clone
	clone.parent = obj_clone_dict.get(obj.parent)
	clone_collec.objects.link(clone)
	clone.light_linking.blocker_collection = clone_collec
	clone.light_linking.receiver_collection = clone_collec

	for child in obj.children:
		clone_object_recursive(src, child, clone_collec, obj_clone_dict)


def clone_props(src, clone, obj_clone_dict):
	for propname in dir(src):
		if '__' not in propname:
			try:
				value = getattr(src, propname)
				if type(value) == bpy.types.Object:
					try:
						setattr(clone, propname, obj_clone_dict[value])
					except KeyError:
						setattr(clone, propname, value)
				else:
					setattr(clone, propname, value)
			except AttributeError:  # probably due to attempting to set a read-only prop
				pass
	return clone


classes = (
	PREFERENCES_PT_grpt,
	StringGroup,
	ObjectGroup,
	GRP_LO_COORD,
	GRP_LO_FIXED,
	GRP_LO_OB,
	GRP_CONFIG,
	UI_UL_grp_config,
	UI_UL_lol_object,
	SCENE_PT_grpt,
	SCENE_PT_grpt_lol,
	SCENE_OT_grp_add,
	SCENE_OT_grp_del,
	SCENE_OT_grp_mov,
	SCENE_OT_grp_get_lob,
	SCENE_OT_grp_overlay_add,
	SCENE_OT_grp_overlay_del,
	SCENE_OT_grp_overlay_mov,
	SCENE_OT_grp_overlay_object_add,
	SCENE_OT_grp_overlay_object_del,
	SCENE_OT_grp_overlay_object_mov,
	SCENE_OT_grp_bmp_output,
	SCENE_OT_grp_output,
)


DESC_GRPT_PMS = '''This is the path that PMS commands will be called on'''
DESC_GRPT_MPQ = '''This is the output path for GRP files'''
DESC_GRPT_PREVIEW = '''Open output images with the system default image previewing program as they are completed'''
DESC_GRPT_PREVIEWONLY = '''While "Preview Output" is on, this option disables the actual GRP file output'''


@persistent
def grpt_pal_init(filepath=''):
	for ii in range(len(bpy.context.scene.grpt_palettes)):
		bpy.context.scene.grpt_palettes.remove(0)

	for filename in glob.glob(bpy.context.scene.grpt_path_pms + '\Palettes\*.act'):
		bpy.context.scene.grpt_palettes.add().name = filename.split('\\')[-1].rsplit(os.extsep, 1)[0]


def grpt_path_pms_get(self):
	return self['grpt_path_pms']


def grpt_path_pms_set(self, value):
	self['grpt_path_pms'] = value
	grpt_pal_init()


def register():
	for cls in classes:
		bpy.utils.register_class(cls)

	bpy.types.Scene.grpt_path_pms = bpy.props.StringProperty(
		options=set(), get=grpt_path_pms_get, set=grpt_path_pms_set, description=DESC_GRPT_PMS
	)
	bpy.types.Scene.grpt_path_mpq = bpy.props.StringProperty(options=set(), description=DESC_GRPT_MPQ)
	bpy.types.Scene.grpt_output_preview = bpy.props.BoolProperty(options=set(), description=DESC_GRPT_PREVIEW)
	bpy.types.Scene.grpt_preview_only = bpy.props.BoolProperty(options=set(), description=DESC_GRPT_PREVIEWONLY)
	bpy.types.Scene.grpt_grp_config = bpy.props.CollectionProperty(type=GRP_CONFIG)
	bpy.types.Scene.grpt_grp_output_index = bpy.props.IntProperty(options=set(), default=-1)
	bpy.types.Scene.grpt_palettes = bpy.props.CollectionProperty(type=StringGroup)
	bpy.types.Scene.grpt_bmp_color_palette = bpy.props.StringProperty(options=set())
	bpy.types.Scene.grpt_bmp_resize_x = bpy.props.IntProperty(options=set(), min=0, max=256, default=0, description=DESC_BMP_RESIZE)
	bpy.types.Scene.grpt_bmp_resize_y = bpy.props.IntProperty(options=set(), min=0, max=256, default=0, description=DESC_BMP_RESIZE)

	bpy.app.handlers.load_post.append(grpt_pal_init)


def unregister():
	for cls in reversed(classes):
		bpy.utils.unregister_class(cls)


if __name__ == '__main__':
	register()
